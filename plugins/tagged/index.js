var util = require("util");
var slugify =  require("slugify")

module.exports = (options, ctx) => ({
    name: 'tagged',
    additionalPages(cx) 
    {
        //console.log("tagged --> additional pages",util.inspect(cx.pages,true,1,true));
       
       let map ={};
        for(let page of cx.pages)
        {
            //console.log(" page \n", page.frontmatter);
            //console.log(" title \n", page.title);
            if(page.frontmatter && 
                page.frontmatter.tags )
            {
                page.frontmatter.tag_details =[];
                for(let tag of page.frontmatter.tags)
                {
                    
                    if(map[tag])
                    {
                        continue;
                    }

                    const content=`
<TagPage tag="${tag}"/>
                    `;

                    let tag_path = '/tag/'+slugify(tag);
                    map[tag] = {
                        path: tag_path,
                        title:`Pages tagged ${tag}`,
                        frontmatter:
                        {
                            no_related_pages:true,
                            is_generated:true
                        },
                        content
                    }

                    page.frontmatter.tag_details.push({link:tag_path, tag})
                    
                }
            }
        }

        let ret=[];
        console.log("mapping of tags ", map);

        for(let tag_page in map)
        {
            ret.push(map[tag_page]);
        }
        console.log("additional pages (tagged) ", ret);

        //throw Error("Stop here now!");
        
        
        ret.push({
            path:'/tag/',
            title:'All tags',
            frontmatter:
            {
                no_related_pages:true
            },
            content:'<AllTags />'
        })
        
        return ret;
    }

}); 