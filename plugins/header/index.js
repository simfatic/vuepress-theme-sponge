module.exports = (options, ctx) => ({
    name: 'header-cdn-loader',
    ready()
    {
        const { siteConfig = {} } = ctx;
        siteConfig.head = siteConfig.head || [];

        let headers = [
            ['link', { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' }],
            
            ['link', { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/prismjs@1.17.1/themes/prism.min.css' }],
    
            ['script', {  src: 'https://cdn.jsdelivr.net/npm/prismjs@1.17.1/prism.min.js' }]
            
        ];
        for(let header of headers)
        {
            siteConfig.head.push(header);
        }
        
        
    }

})