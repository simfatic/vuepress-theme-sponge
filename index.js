const path = require('path')

module.exports = function(opts, ctx)
{
  var options = 
  {
    name: 'vuepress-theme-sponge',

    plugins: [
      [
        '@vuepress/register-components', {
        componentsDir: [
          path.resolve(__dirname, 'components')
        ]
      }],
      require("./plugins/header"),
      require("./plugins/tagged"),
      require("./plugins/categories")
    ],

    enhanceAppFiles: [
      path.resolve(__dirname, 'enhanceApp.js'),
    ]
  }

  return options;
}
