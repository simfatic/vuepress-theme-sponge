# vuepress-theme-sponge

A vuepress theme for Tech blogs (still under development.)

## Features 

- Uses Bootstrap and loads it from CDN 
- Loads all main dependencies from CDNs rather than compiling in to one CSS/JS so that to enhance speed and to save bandwidth
- a standard layout with header, sidebar and footer
