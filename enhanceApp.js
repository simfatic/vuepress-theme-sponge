export default function(param)
  {
    let { Vue,options, siteData} = param;
    
    Vue.mixin({
        computed: 
        {
          $title() 
          {
            let title ="";
            if(this.$page.frontmatter.home)
            {
                title =  this.$siteTitle;
            }
            else if(this.$page.title)
            {
                title = this.$page.title;
            }
            else if(this.$page.frontmatter.title )
            {
                title = this.$page.frontmatter.title;
            }
            if(this.$site.themeConfig.append_title)
            {
              return title+' | '+this.$site.themeConfig.site_name;
            }
            return title;
          }
        }
      });
      
  }