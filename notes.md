===TODOs===

Ability to add logo image to the header
Abilty to add Google ads

Side bar-> add more links? (featured, category-wise. List all categories)

can't sort by post date. See if front matter date can be used for the sort


Move  plugins to seperate packages

Make it possible to make many themes using same plugins

Add thumbnail? featured image? fastwebstart would need that.

References:

https://v1.vuepress.vuejs.org/plugin/official/plugin-last-updated.html

Example vuepress themes for reference
https://github.com/meteorlxy/vuepress-theme-meteorlxy

Adding tags to vuepress
https://code.roygreenfeld.com/cookbook/adding-tags-to-vuepress.html

Additional Pages Plugin API (can be used to add tag pages and category pages)
https://vuepress.vuejs.org/plugin/option-api.html#additionalpages